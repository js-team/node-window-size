# Installation
> `npm install --save @types/window-size`

# Summary
This package contains type definitions for window-size (https://github.com/jonschlinkert/window-size).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/window-size.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/window-size/index.d.ts)
````ts
// Type definitions for window-size 1.1.1
// Project: https://github.com/jonschlinkert/window-size
// Definitions by: Pouya Kary <https://github.com/pmkary>,
//                 Juer Whang <https://github.com/juergenie>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference types="node" />

declare module 'window-size' {
    import { WriteStream } from 'fs';

    const windowSize: windowSize.Size & {
        /** Get terminal window's size with available channels. */
        get(options?: windowSize.WindowSizeOptions): windowSize.Size;
        /** Get terminal window's size with `process.env.COLUMNS` and `process.env.ROWS`. */
        env(): windowSize.Size;
        /** Get terminal window's size with `tty` module */
        tty(options: windowSize.TtySizeOptions): windowSize.Size;
        tput(): windowSize.Size;
        win(): windowSize.Size;
    };
    export = windowSize;
    namespace windowSize {
        export interface Size {
            width: number;
            height: number;
            type: string;
        }

        /** Options of inner function `streamSize`. */
        type StreamSizeOptions = Record<string, WriteStream>;

        /** Options of function `windowSize.tty`. */
        interface TtySizeOptions {
            tty?: {
                getWindowSize?: (out: WriteStream) => [number, number];
            };
        }

        /** Options of function `windowSize.get` */
        type WindowSizeOptions = StreamSizeOptions & TtySizeOptions;
    }
}

````

### Additional Details
 * Last updated: Tue, 07 Sep 2021 01:01:18 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Pouya Kary](https://github.com/pmkary), and [Juer Whang](https://github.com/juergenie).
